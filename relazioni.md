# RELAZIONE
  
Ogni studente dovrà presentare una relazione relativa ai laboratori svolti entro **5gg LAVORATIVI** dalla data in cui è previsto l’esame orale.  

---
Prendiamo ad esempio l' esame previsto per il giovedì 10/02.   
 
|04 Febbraio|05 Febbraio|06 Febbraio|07 Febbraio|08 Febbraio|09 Febbraio|10 Febbraio|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|Venerdì|Sabato|Domenica|Lunedì|Martedì|Mercoledì|Giovedì|
|LAVORATIVO|NON LAVORATIVO|NON LAVORATIVO|LAVORATIVO|LAVORATIVO|LAVORATIVO|**ESAME**|
|**LIMITE ULTIMO PER LA CONSEGNA**|4|3|2|1|1|1|
---

La relazione dovrà riguardare **tutte** le seguenti tematiche:
1. Descrizione della procedura bioinformatica relativa all’analisi di dati di DNA-Metabarcoding e discussione dei dati ottenuti:
   1. Vanno comparate sia le metodologie bioinformatiche che i risultati ottenuti durante l' esercitazione con quelli presentati nel lavoro [Ravegnini et al. 2020](https://www.mdpi.com/1422-0067/21/24/9735);  
2. Descrizione della procedura bioinformatica per l’assemblaggio e annotazione di un genoma batterico corredata da un breve commento dei risultati ottenuti.
   1. Tra le sequenze proteiche identificate da **PROKKA** sceglierne una e utilizzando uno dei tool che abbiamo visto durante le esercitazioni verificare che la predizione funzionale sia corretta.  
  
3. Descrizione della procedura bioinformatica relativa all’analisi di dati di Trascrittomica e discussione dei dati ottenuti:  
    1. Vanno comparate sia le metodologie bioinformatiche che i risultati ottenuti durante l' esercitazione con quelli presentati nel lavoro [Annese et al. 2018, Scientific Reports](https://www.nature.com/articles/s41598-018-22701-2);  
    2. Tra i geni differenzialmente espressi va scelto uno ed indicate le seguenti informazioni:  
        - Simbolo ufficiale del Gene;  
        - Identificativo del Gene;  
        - Localizzazione cromosomica comprensiva di coordinate e strand;  
        - Numero di trascritti annotati;  
            - Considerando il trascritto codificante proteina più lungo dovete indicare:  
                * l' identificativo del trascritto;  
                * il numero di Esoni che partecipano alla formazione di quel trascritto;  
        - Numero di Ortologhi annotati.  
        - Utilizzando la sequenza proteica identificate la presenza di possibile proteine omologhe.  

***Per ciascuna delle tre tematiche non dovranno superare le 5 pagine e per ciascuna di esse potete inserire al massimo 5 immagini***.  

[Back to the top](../README.md) 