# [GISAID](https://www.gisaid.org)
![](gisaid.png)  
L'iniziativa **GISAID** promuove la rapida condivisione dei dati relatici ai virus influenzali e del coronavirus che 
causa la patologia nota come ***COVID-19***.  
Ciò include la sequenza genetica e i relativi dati clinici ed epidemiologici associati ai virus umani, nonché dati 
geografici e specie specifici associati a virus aviari e ad altri virus animali, per aiutare i ricercatori a capire 
come i virus si evolvono e si diffondono durante epidemie e pandemie.

L'accesso ai dati (download) è regolato da un _agreement_ che richiede una iscrizione. 

GISAID è un valido strumento per capire come un genoma virale si stia evolvendo nel tempo e studiarne le sue caratteristiche
in funzione alle aree geografiche.  
Nella home page troverete:  
 - Un focus sulle notizie principali;  
 - Gli ultimi genomi depositati;  
 - Un tool interattivo per visualizzare una l'epidemiologia del genoma del **hCoV-19**;
 - Un tool che mappa i numeri di casi nel mondo;  
 
Nella sezione **References** possiamo accedere al genoma di riferimento in GISAID per hCoV-19.  

***È lo stesso genoma che abbiamo ottenuto dalla banca dati Genome di NCBI?***  

Per ciascuna delle proteine codificate dal genoma di hCoV-19 è possibile ottenere le informazioni generali ed i lavori scientifici
che le hanno descritte.  

Tornando alla pagina principale possiamo utilizzare il tool **Genomic Epidemiology of hCoV-19**.  


[Back to the top](../README.md)